#include <socket/tcp_socket.hpp>
#include <sys/socket.h>
#include <unistd.h>
#include <errors.hpp>
#include <string>
#include <iostream>


namespace soc {
	void tcp_socket_client::soc_send(const std::string &message) {
		int number_writen { 0 };
		if ((number_writen = write(this->master_fd, message.c_str(), message.length())) == -1 ) {
			THROW_SOCKET_ERROR(
					std::string("Can't write to fd (") + 
					std::to_string(this->master_fd) + 
					"), the number_writen is: " + 
					std::to_string(number_writen)
					);
		}
	}


	const std::string& tcp_socket_client::soc_read() {	
		read(this->master_fd, this->buffer, this->buffer_size);

		return *(new std::string(buffer));
	}

	void tcp_socket_client::init() {
		if ((status = connect(this->master_fd, (sockaddr*)&this->master_addr, this->master_addr_size)) < 0) {
			THROW_SOCKET_ERROR("Can't connect socket");
		}

		this->connected_fd = this->master_fd;
		this->connected_addr = this->master_addr;
	}
}
