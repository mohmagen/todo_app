#include <socket/tcp_socket.hpp>
#include <sys/socket.h>
#include <unistd.h>
#include <errors.hpp>
#include <string>
#include <iostream>


namespace soc {
	void tcp_socket_server::soc_send(const std::string &message) {
		wait_for_connection();

		int number_writen { 0 };
		if ((number_writen = write(this->connected_fd, message.c_str(), message.length())) == -1 ) {
			THROW_SOCKET_ERROR(
					std::string("Can't write to fd (") + 
					std::to_string(this->connected_fd) + 
					"), the number_writen is: " + 
					std::to_string(number_writen)
					);
			close(this->connected_fd);
			this->connected_fd = -1;
		}
	}

	const std::string& tcp_socket_server::soc_read() {	
		wait_for_connection();

		read(this->connected_fd, this->buffer, this->buffer_size);


		return *(new std::string(buffer));
	}

	void tcp_socket_server::init() {
		if ((bind(this->master_fd, (sockaddr*)&this->master_addr, this->master_addr_size)) < 0) {
			THROW_SOCKET_ERROR("Can't bind socket.")
		}
		
		if (listen(this->master_fd, 3) < 0) {
			THROW_SOCKET_ERROR("Can't listen socket");
		}
	}

	void tcp_socket_server::wait_for_connection() {
		if (this->connected_fd == -1) {
			std::cerr << "Can't send to connected. (No connected socket), wait for connection..." << std::endl;

			if ((this->connected_fd = accept(
					this->master_fd, 
					(sockaddr*)&this->connected_fd, 
					(socklen_t*)&this->connected_addr_size
					)) == -1) {
				THROW_SOCKET_ERROR("Can't accept socket");	
			}
		}

	}
}
