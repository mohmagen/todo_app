#include "errors.hpp"
#include <cstring>
#include <memory>
#include <ostream>
#include <socket/msg_socket.hpp>
#include <socket/complex_tcp_socket.hpp>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>

#define _msg_socket_ "\033[36m[ msg socket ]\033[0m"
#define _msg_data_ "\033[36m[ msg data ]\033[0m"

namespace soc {
	///
	/// msg socket
	///
	
	void msg_connected_socket::send_soc(const i_data &data_non_casted) {
		if (typeid(*&data_non_casted) != typeid(msg_data)) {
			_throw_write_error_("data msut be msg_data class");
		}

		auto data = data_non_casted.serialize();
		size_t data_size = strlen(data.get());

		std::cout << _msg_socket_ << " data to send \"" << data << "\"." << std::endl;

		int number_writen { 0 };
		if ((number_writen = send(this->fd, data.get(), data_size, 0)) <= 0) {
			_throw_write_error_(std::string("Can't send data: \'") + data.get() + "\', to " +
					 inet_ntoa(this->addr.sin_addr) + " :: " + std::to_string(this->addr.sin_port));
		}
	}

	const i_data& msg_connected_socket::read_soc() {
		int number_read { 0 };
		if ((number_read = recv(this->fd, this->buffer.get(), this->buffer_size, 0)) <= 0) {
			_throw_read_error_(std::string("Can't recieve data: ") + 
					inet_ntoa(this->addr.sin_addr) + " :: " + std::to_string(this->addr.sin_port));
		}

		std::cout << _msg_socket_ << " recieved data \"" << this->buffer.get() << "\"." << std::endl;

		msg_data *data = new msg_data();
		data->deserialize(this->buffer, this->buffer_size);
		return *data;
	}

	msg_connected_socket::~msg_connected_socket() {
		if (this->fd != -1) {
			close(this->fd);
		}
	}



	///
	/// msg data
	///
	
	const std::shared_ptr<char[]> msg_data::serialize() const noexcept {
		std::shared_ptr<char[]> buffer (new char[1024]);

		for (int i = 0; i < 1024; i++) {
			buffer[i] = '\0';
		}	


		strcpy(buffer.get(), "MSG");
		strcat(buffer.get(), this->name.c_str());
		strcat(buffer.get(), "#");
		strcat(buffer.get(), this->message.c_str());


		return buffer;
	}

	void msg_data::deserialize(const std::shared_ptr<char[]> input, size_t size) {	
		size_t i = 3;
		if (input[0] != 'M' || input[1] != 'S' || input[2] != 'G') {
			_throw_deserialize_error_(std::string("MSG protocol must start with \"MSG\", but input is : \"") 
						+ input.get() + "\".");
		}
		bool name_ended = false;
		for(; i < size; i++) {
			if (input[i] == '#') {
				name_ended = true;
				i++;
				break;
			}

			this->name.push_back(input[i]);
		}

		if (!name_ended) {
			_throw_deserialize_error_(
					std::string("name and message must be separated by '#' character, but they don't. input is: \"")
					+ input.get() + "\".");
		}

		for(; i < size; i++) {
			this->message.push_back(input[i]);
			
			if (input[i] == '\0') {
				break;
			}
		}
	}
}
