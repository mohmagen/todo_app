#include <errors.hpp>
#include <string.h>


const char socker_error[] = __socket_error__;

socket_error::socket_error(
		const char* file, 
		int line, 
		const char* message
		) 
	: message( 
			std::string(socker_error) + 
			"(" + file + ":" + 
			std::to_string( line ) + 
			")" + message + "\'" 
			) {}

socket_error::socket_error(
		const char* file, 
		int line, 
		std::string message
		) 
	: message( 
			std::string(socker_error) + 
			"(" + file + ":" + 
			std::to_string( line ) + 
			")" + message + "\'" 
			) {}

const char* socket_error::what() const noexcept {
	return this->message.c_str();
}
