#include "errors.hpp"
#include "socket/msg_socket.hpp"
#include <arpa/inet.h>
#include <socket/complex_tcp_socket.hpp>
#include <atomic>
#include <cstdint>
#include <memory>
#include <expected>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <iostream>
#include <algorithm>
#include <unistd.h>


namespace soc {
	f_complex_tcp_server_socket_owner::f_complex_tcp_server_socket_owner(
			const std::string &in_address, size_t port, size_t max_clients) noexcept 
	{
		this->addr.sin_port = htons(port);
		this->addr.sin_family = AF_INET;
		this->addr.sin_addr.s_addr = inet_addr(in_address.c_str());
		this->fd = -1;

		this->max_clients = max_clients;

		this->connected_sockets = std::list<std::shared_ptr<i_complex_tcp_socket_connected>> ();
	}	

	f_complex_tcp_server_socket_owner::~f_complex_tcp_server_socket_owner() {
		while (!this->connected_sockets.empty()) {
			auto curr = this->connected_sockets.back();
			this->connected_sockets.pop_back();

			if (!curr.unique()) {
				std::cerr << "socket has other owners" << std::endl;
			}
			curr.reset();
		}

		shutdown(this->fd, SHUT_RDWR);
	}

	void f_complex_tcp_server_socket_owner::init_soc() {
		if((this->fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			_throw_socket_error_("can't create socket");	
		}

		if((bind(this->fd, (sockaddr*)&this->addr, this->addr_size)) < 0) {
			_throw_socket_error_(std::string("can't bind socket on address") 
					+ inet_ntoa(this->addr.sin_addr) + " :: " + std::to_string(this->addr.sin_port));
		}
	}

	void f_complex_tcp_server_socket_owner::listen_soc() {
		if((listen(this->fd, this->max_clients)) < 0) {
			_throw_socket_error_("can't listen socket");
		}
	}

	std::shared_ptr<i_complex_tcp_socket_connected> f_complex_tcp_server_socket_owner::
	create_connected(e_connected_type connected_type) {
		

		int accepted_fd;
		sockaddr_in connected_addr;
		size_t connected_addr_size { sizeof(connected_addr) };

		if ((accepted_fd = accept(this->fd, (sockaddr*)&connected_addr, (socklen_t*)&connected_addr_size)) < 0) {
			_throw_socket_error_("can't accept socket");	
		}

		std::shared_ptr<i_complex_tcp_socket_connected>	result = 
			std::make_shared<msg_connected_socket>(accepted_fd, connected_addr);

		this->connected_sockets.push_back(result);

		// switch (connected_type) {
		// 	case e_connected_type::MSG:
		// 		result = std::make_shared<i_complex_tcp_socket_connected>
		// 			(new msg_connected_socket(accepted_fd, connected_addr));
		// 		break;
		// 	case e_connected_type::FTP:
		// 		///! TODO
		// 		result = std::make_shared<i_complex_tcp_socket_connected>
		// 			(new msg_connected_socket(accepted_fd, connected_addr));
		// 		break;
		// 	case e_connected_type::JNTP:
		// 		///! TODO
		// 		result = std::make_shared<i_complex_tcp_socket_connected>
		// 			(new msg_connected_socket(accepted_fd, connected_addr));
		// 		break;
		// }

		return result;
	}

}
