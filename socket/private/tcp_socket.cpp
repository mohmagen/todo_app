#include <arpa/inet.h>
#include <netinet/in.h>
#include <socket/tcp_socket.hpp>
#include <sys/socket.h>
#include <unistd.h>
#include <errors.hpp>
#include <string>
#include <iostream>


namespace soc {
	tcp_socket::tcp_socket(size_t port, const std::string &in_address) {
		if ((this->master_fd = socket(AF_INET, SOCK_STREAM, 0))	< 0) {
			THROW_SOCKET_ERROR(std::string("Can't create socket at:") + in_address + "(" + std::to_string(port) + ")");
		}

		this->master_addr.sin_port = htons(port);
		this->master_addr.sin_addr.s_addr = inet_addr(in_address.c_str());
		this->master_addr.sin_family = AF_INET;
	}

	tcp_socket::~tcp_socket() {
		std::cout << "tcp socket destructor" << std::endl;
		close(this->connected_fd);
		shutdown(this->master_fd, SHUT_RDWR);
	}

	std::string tcp_socket::get_socket_data() const {
		return 
			std::string("The master socket is ") + 
			"socket id" + std::to_string(this->master_fd) + ", " +
			"port" + std::to_string(this->master_addr.sin_port) + ":" +
			"connected socket" + 
			"socket id" + std::to_string(this->connected_fd) + ", " + 
			"port" + std::to_string(this->connected_addr.sin_port) + ".";
	}	
}
