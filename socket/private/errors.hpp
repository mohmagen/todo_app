#pragma once

#include <error.h>
#include <exception>
#include <string>

#define __throw_cerr__ 1


class socket_error: std::exception {
protected:
	std::string message;
public:
	socket_error(const char* file, int line, const char* message);
	socket_error(const char* file, int line, std::string message);

	virtual const char* what() const noexcept;
};

#define __add_socket_error_inherits__(name)\
	class name##_error : socket_error {\
		public:\
			name##_error(const char* file, int line, const char* message) : socket_error(file, line, message) {}\
			name##_error(const char* file, int line, std::string message) : socket_error(file, line, message) {}\
	}
__add_socket_error_inherits__(write);
__add_socket_error_inherits__(read);
__add_socket_error_inherits__(cast);
__add_socket_error_inherits__(deserialize);


#define __socket_error__ "\x1B[31m[SocketError]\033[0m: \'"

#define THROW_SOCKET_ERROR(message)\
	if(__throw_cerr__)\
		std::cerr << __socket_error__ << "[" __FILE__ ":" << __LINE__ << "]:" << message << std::endl;\
	throw new socket_error(__FILE__, __LINE__, message);

#define _throw_socket_error_(message)\
	if(__throw_cerr__)\
		std::cerr << __socket_error__ << "[" __FILE__ ":" << __LINE__ << "]:" << message << std::endl;\
	throw new socket_error(__FILE__, __LINE__, message);

#define _throw_write_error_(message)\
	if(__throw_cerr__)\
		std::cerr << __socket_error__<< "[" __FILE__ ":" << __LINE__ << "]:" << message << std::endl;\
	throw new write_error(__FILE__, __LINE__, message)

#define _throw_read_error_(message)\
	if(__throw_cerr__)\
		std::cerr << __socket_error__<< "[" __FILE__ ":" << __LINE__ << "]:" << message << std::endl;\
	throw new read_error(__FILE__, __LINE__, message)

#define _throw_cast_error_(message)\
	if(__throw_cerr__)\
		std::cerr << __socket_error__<< "[" __FILE__ ":" << __LINE__ << "]:" << message << std::endl;\
	throw new cast_error(__FILE__, __LINE__, message)

#define _throw_deserialize_error_(message)\
	if(__throw_cerr__)\
		std::cerr << __socket_error__<< "[" __FILE__ ":" << __LINE__ << "]:" << message << std::endl;\
	throw new deserialize_error(__FILE__, __LINE__, message)
