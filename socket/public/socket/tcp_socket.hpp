#pragma once 

#include <netinet/in.h>
#include <socket/i_socket.hpp>
#include <sys/socket.h>
#include <string>


namespace soc {
	class tcp_socket: public i_socket {
		public:
			tcp_socket(size_t port, const std::string &in_address);
			virtual ~tcp_socket();

			virtual std::string get_socket_data() const override;

		protected:
			int master_fd { -1 };
			int connected_fd { -1 };

			sockaddr_in master_addr;
			sockaddr_in connected_addr;

			size_t master_addr_size { sizeof(sockaddr_in) };
			size_t connected_addr_size { sizeof(sockaddr_in) };
				
			char buffer[1024];
			size_t buffer_size { sizeof(buffer) };
	};

	class tcp_socket_server: public tcp_socket {
		public:
			tcp_socket_server(size_t port, const std::string &message) : tcp_socket(port, message) {}
			

		public: // override from i_socket
			virtual void soc_send(const std::string &message) override;
			virtual const std::string& soc_read() override;

			virtual void init() override;

		private:
			void wait_for_connection();
	};

	class tcp_socket_client: public tcp_socket {
		public:
			tcp_socket_client(size_t port, const std::string &message) : tcp_socket(port, message) {}

		public: // override from i_socket
			virtual void soc_send(const std::string &message) override;
			virtual const std::string& soc_read() override;

			virtual void init() override;
		
		private:
			int status;
	};
}
