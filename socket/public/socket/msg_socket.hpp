#pragma once

#include <atomic>
#include <memory>
#include <netinet/in.h>
#include <socket/complex_tcp_socket.hpp>
#include <sys/socket.h>


namespace soc {
	class msg_connected_socket : public i_complex_tcp_socket_connected {
		public:
			void send_soc(const i_data &data)  override;
			const i_data &read_soc()  override;

			msg_connected_socket(int accepted_fd, sockaddr_in addr) noexcept
				: fd(accepted_fd), addr(addr), buffer(new char[ 1024 ]){}

			~msg_connected_socket();

		private:
			int fd;
			sockaddr_in addr;

			std::shared_ptr<char[]> buffer;
			size_t buffer_size { 1024 };
	};

	class msg_data : public i_data {
		public:
			msg_data() : name(""), message("") {}
			msg_data(const std::string &name, const std::string &message) : name(name), message(message) {}

			const std::shared_ptr<char[]> serialize() const noexcept override;
			void deserialize(const std::shared_ptr<char[]> input, size_t size) override;

			std::string name, message;
	};
}
