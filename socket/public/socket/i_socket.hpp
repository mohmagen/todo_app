#pragma once

#include <string>

namespace soc {
	class i_socket {
		public:
			virtual void soc_send(const std::string &message) = 0;
			virtual const std::string& soc_read() = 0;


			virtual void init() = 0;

			virtual std::string get_socket_data() const = 0;
	};
}
