#pragma once


#include <atomic>
#include <cstdint>
#include <memory>
#include <expected>
#include <netinet/in.h>
#include <string>
#include <list>

namespace soc {
	/// types of accepted sockets
	enum class e_connected_type: int8_t {
		/// Jaba Notes Transfer Protocol (protocol to send jabby notes).
		JNTP,

		/// File Transfer Protocol (send files).
		FTP,

		/// Simple messages
		MSG,
	};

	class i_complex_tcp_socket_connected;
	class i_data;


	/// fabric for connected sockets and owner socket
	class f_complex_tcp_server_socket_owner {
		public:
			f_complex_tcp_server_socket_owner(const std::string &in_address, size_t port, size_t max_clients) noexcept;
			~f_complex_tcp_server_socket_owner();

			/// create socket
			void init_soc();

			/// listen socket 
			void listen_soc();

			/// fabric to create *complex_tcp_socket_connected* with chosen 
			/// application protocol type.
			/// 
			/// @connected_type type of connected socket
			///
			/// @return accepted socket :: *complex_tcp_socket_connected*
			std::shared_ptr<i_complex_tcp_socket_connected>
				create_connected(e_connected_type connected_type);
		private:
			int fd;
			sockaddr_in addr;
			size_t addr_size { sizeof(addr) };
			size_t max_clients;
			
			std::list<std::shared_ptr<i_complex_tcp_socket_connected>> connected_sockets;
	};

	/// interface for connected sockets
	class i_complex_tcp_socket_connected {
		public:
			virtual void send_soc(const i_data &data) = 0;
			virtual const i_data &read_soc() = 0;
	};

	/// interface for data
	class i_data {
		public:
			virtual const std::shared_ptr<char[]> serialize() const noexcept = 0;
			virtual void deserialize(const std::shared_ptr<char[]> input, size_t size)  = 0;
	};

}
