#pragma once 

#include <memory>
namespace ctrl {
	class controller_ptr;

	/// controller or bridge between backend and frontend
	class controller {
			controller() {};
		public:
			[[nodiscard]] static controller_ptr new_atomic_controller();
			
			/// start app (both view and model parts)
			void start();

			static void exit();
			static void todo_list();
			static void todo_add();
			static void todo_start();
			static void todo_complete();
			static void todo_remove();
			static void todo_open();
			static void todo_description();
			static void todo_connect();
			static void tood_pull();
			static void todo_push();
			static void server_connect();
			static void server_pull();
			static void server_push();
	};


	class controller_ptr {
		std::shared_ptr<controller> ptr;

		friend controller;
		controller_ptr(std::shared_ptr<controller> ptr) 
			: ptr(ptr) {}

		public:
			[[nodiscard]] std::shared_ptr<controller> load();
	};
}
