#include <atomic>
#include <controller.hpp>
#include <memory>

namespace ctrl {
	controller_ptr controller::new_atomic_controller() {
		std::shared_ptr<controller> ptr (new controller());
		return controller_ptr(ptr);
	}

	std::shared_ptr<controller> controller_ptr::load() {
		return std::atomic_load(&this->ptr);
	}
}
