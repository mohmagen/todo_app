#include "commands.hpp"
#include "controller.hpp"
#include "errors.hpp"
#include <algorithm>
#include <iterator>
#include <memory>
#include <string>
#include <sstream>
#include <tuple>
#include <unordered_set>
#include <utility>
#include <vector>
#include <view.hpp>
#include <iostream>
#include <log_help.hpp>
#include <optional>

std::pair<std::string, std::vector<std::string>> parse_command(std::string command);

namespace view {

	console::console(ctrl::controller_ptr controller) 
		: state(new console_state()), commands_owner(new f_commands_owner(state, controller)) {}

	void console::start() {
		std::string input;

		std::cout << _todo_app_ << "input something..." << std::endl;
		while (!this->state->exit) {
			_input_(input);
		

			try {
				auto [command_name, arguments] = parse_command(input);

				this->commands_owner->get_command(command_name)->execute(arguments);
			}
			catch (err::wrong_command_view_error *) {
				std::cout << _todo_app_ << "write help to get available commands list." << std::endl;
			} 
			catch (err::command_arguments_view_error *) {
				std::cout << _todo_app_ << "to few arguments." << std::endl;	
			}
		}
	}	

}

std::pair<std::string, std::vector<std::string>> parse_command(std::string command) {	

	std::stringstream input_stream (command);

	std::string name;
	input_stream >> name;	

	std::vector<std::string> arguments;
	std::string arg, res;
	std::optional<std::string> wait_for { std::nullopt };
	static const std::unordered_map<std::string, std::string> bounds = {
		{"\"", "\""}, {"\'", "\'"}, {"(", ")"}, {"[[", "]]"}, {"<", ">"}
	};

	while (input_stream >> arg) {
		if (wait_for) {	
			res += " ";
			res += arg;

			if (std::equal(wait_for->rbegin(), wait_for->rend(), arg.rbegin())) {	
				arg = arg.substr(arg.length() - wait_for->length(), arg.length());
				wait_for = std::nullopt;
			}
		} else {
			res = arg;

			auto begin_with_bound = std::find_if(bounds.begin(), bounds.end(), [&arg](auto bound){
					return std::equal(bound.first.begin(), bound.first.end(), arg.begin());
				});

			if (begin_with_bound != bounds.end()) {
				arg = arg.substr(begin_with_bound->first.length(), arg.length());
				wait_for = std::optional(begin_with_bound->second);
				
				if (std::equal(wait_for->rbegin(), wait_for->rend(), arg.rbegin())) {	
					arg = arg.substr(arg.length() - wait_for->length(), arg.length());
					wait_for = std::nullopt;
				}
			}
		}


		if (!wait_for) {
			arguments.push_back(res);
		}
	}

	if (wait_for) {
		_throw_error_(command_parse_error, std::string("unclosed bound \"") + wait_for->c_str() + "\"");
	}
	
	return {name, arguments};
}
