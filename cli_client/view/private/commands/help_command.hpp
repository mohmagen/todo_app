#pragma once 

#include "view.hpp"
#include <commands.hpp>
#include <string>
#include <unordered_map>
#include <vector>

namespace view {
	namespace commands {
		struct help_command : public i_command {
			public:
				help_command(
						command_definition definition, 
						std::shared_ptr<console_state> state, 
						ctrl::controller_ptr controller, 
						std::shared_ptr<commands_definitions_map> commands_definitions)
					: i_command(definition, state, controller), commands_definitions(commands_definitions) {}
				~help_command() {}

				virtual void execute(std::vector<std::string>) override;

			private:
				std::shared_ptr<commands_definitions_map> commands_definitions;
		};
	}
}
