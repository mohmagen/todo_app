#include "controller.hpp"
#include <commands.hpp>


namespace view {
	namespace commands {
		struct todo_command: public i_command {	
			public:
				todo_command(
						command_definition definition, 
						std::shared_ptr<console_state> state, 
						ctrl::controller_ptr controller)
					: i_command(definition, state, controller) {}
				~todo_command() {}

				virtual void execute(std::vector<std::string>) override;
		};
	}
}
