#include "help_command.hpp"
#include <log_help.hpp>
#include <string>
#include <vector>
#include <iostream>

namespace view {
	namespace commands {
		void help_command::execute(std::vector<std::string> arguments) {
			for (auto [name, description] : *this->commands_definitions)	{
				std::cout << _indent_ << name << " - " << description.description << std::endl;
			}
		}
	}	
}
