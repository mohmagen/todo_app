#include "exit_command.hpp"
#include "errors.hpp"
#include "log_help.hpp"
#include <vector>
#include <view.hpp>
#include <iostream>


void view::commands::exit_command::execute(std::vector<std::string> arguments) {
	if (arguments.size() > 0) {
		_throw_error_(command_arguments, "exit command have no arguments.");
	}

	std::cout << _todo_app_ << "exit ..." << std::endl;

	this->state->exit = true;
}
