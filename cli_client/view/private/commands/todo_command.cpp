#include "todo_command.hpp"
#include "errors.hpp"
#include "log_help.hpp"
#include <vector>
#include <view.hpp>
#include <iostream>


void view::commands::todo_command::execute(std::vector<std::string> arguments) {
	if (arguments.size() > 0) {
		_throw_error_(command_arguments, "`todo' command have no arguments.");
	}

	auto controller = this->controller.load();
	controller->todo_list();
}
