#include "commands/help_command.hpp"
#include "commands/exit_command.hpp"
#include "errors.hpp"
#include <algorithm>
#include <commands.hpp>
#include <memory>
#include <log_help.hpp>
#include <ostream>
#include <string>
#include <vector>
#include <view.hpp>

namespace view {

	f_commands_owner::~f_commands_owner() {
	}

	std::shared_ptr<commands::i_command> f_commands_owner::get_command(const std::string &name) const {
		auto definition = get_command_definition(name);

		if (name == "help") {
			return std::shared_ptr<commands::help_command> 
				(new commands::help_command( definition, this->state, this->controller, this->commands_definitions));
		}

		if (name == "exit") {
			return std::shared_ptr<commands::exit_command>
				(new commands::exit_command(definition, this->state, this->controller));
		}

		return std::shared_ptr<commands::i_command> 
			(new commands::i_command(this->get_command_definition(name), this->state, this->controller));
	}
	
	command_definition f_commands_owner::get_command_definition(const std::string &name) const {
		if (!this->commands_definitions->contains(name)) {
			_throw_error_(wrong_command, std::string("`") + name + "\' isn't command.");		
		}

		return this->commands_definitions->at(name);
	}

	void commands::i_command::execute(std::vector<std::string> arguments) {
		std::cout << _todo_app_ << "run command " << _str_(this->definition.name) 
			<< " " << _str_(this->definition.description) << ", arguments is [ ";

		std::for_each(arguments.begin(), arguments.end(), [](auto arg) { std::cout << _str_(arg) << " " ; });

		std::cout << "]" << std::endl;
	}
	
	commands::i_command::~i_command() {}


	f_commands_owner::f_commands_owner(std::shared_ptr<console_state> state, ctrl::controller_ptr controller) 
		: state(state), controller(controller), commands_definitions(new commands_definitions_map {
				{"exit", {"exit", "exit from todo app"}},
				{"help", {"help", "print list of available commands"}},
				{"todo-list", {"todo-list", "print todo list."}},
				{"todo-add", {"todo-add", "add task to todo list."}},
				{"todo-start", {"todo-start", "start doing a task (make task in process state)."}},
				{"todo-complete", {"todo-complete", "complete task."}},
				{"todo-remove", {"todo-remove", "remove task from todo list."}},
				{"todo-open", {"todo-open", "open task in editor (to view and change description)."}},
				{"todo-description", {"todo-description", "print description of task."}},
				{"server-connect", {"server-connect", "connect to server."}},
				{"server-pull", {"servp-pull",  "get data from server. (automaticly run after connect to server)"}},
				{"server-push", {"server-push", "synchronise data with server. (automaticly run when exeit todo app)"}}, 
				}) {}
}
