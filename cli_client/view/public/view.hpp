#pragma once

#include "commands.hpp"
#include "controller.hpp"
#include <memory>
#include <string>
#include <unordered_set>

namespace view {
	struct console_state {
		bool exit { false };	
	};

	class console {
		public:
			std::shared_ptr<console_state> state;

			console(ctrl::controller_ptr controller);

			void start();
		private:
			std::unique_ptr<f_commands_owner> commands_owner;
	};

}
