#pragma once 

#include <exception>
#include <string>
#include <iostream>
#include <log_help.hpp>

#define _throw_error_(type, message)\
	{\
		auto *err = new err::type##_view_error(__FILE__, __LINE__, message);\
		std::cerr << err->what() << std::endl;\
		throw err;\
	}


#define _new_error_(type)\
	struct type##_view_error : public view_error {\
		type##_view_error(const char *file, int line, const std::string &message)\
			: view_error(file, line, message) {}\
	}

#define __error__ "\033[31m[ view error ]\033[0m "


namespace err {
	struct view_error: public std::exception {
		std::string message;
		view_error(const char *file, int line, const std::string &message): 
			message(std::string(__error__ "\n\t" _italic_ _white_ " ") + file + " ] ( " 
					+ std::to_string(line) + " )\033[0m\n\t"
					+ _str_styles_(message, _red_ _bold_) + "\n") {}

		virtual const char *what() const noexcept override {
			return this->message.c_str();	
		}

	};

	_new_error_(out_of_range);
	_new_error_(wrong_command);
	_new_error_(command_parse_error);
	_new_error_(command_arguments);
}





