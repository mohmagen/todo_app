#pragma once 
#include "controller.hpp"
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace view {
	struct command_definition {
		std::string name;
		std::string description;
	};

	using commands_definitions_map = std::unordered_map<std::string, command_definition>;

	struct console_state;

	namespace commands {
		struct i_command {
			i_command(
					command_definition definition, 
					std::shared_ptr<console_state> state, 
					ctrl::controller_ptr controller) 
				: definition(definition), state(state), controller(controller){}

			virtual ~i_command();
			
			virtual void execute(std::vector<std::string> arguments);
			protected:
				command_definition definition;
				std::shared_ptr<console_state> state;
				ctrl::controller_ptr controller;
		};

	}

	class f_commands_owner {
		std::shared_ptr<console_state> state;
		ctrl::controller_ptr controller;

		public:
			f_commands_owner(std::shared_ptr<console_state>, ctrl::controller_ptr);
			~f_commands_owner();

			std::shared_ptr<commands::i_command> get_command(const std::string &name) const;

		private:
			command_definition get_command_definition(const std::string &name) const;

			std::shared_ptr<commands_definitions_map> commands_definitions;	
	};
}
