#pragma once

#define _input_arrows_ "\033[94m[ input ]\033[0m \033[36m<<\033[0m "
#define _input_(output) \
	std::cout << _input_arrows_;\
	std::getline(std::cin, output)

#define _indent_ "\t"

#define _todo_app_ "\033[33m[ todo app ]\033[0m "
#define _str_(str) "\"" << str << "\""



#define _str_styles_(message, style) (std::string(style) + message + _end_)

#define _black_		"\033[30m"
#define _red_		"\033[31m"
#define _green_		"\033[32m"
#define _yellow_	"\033[33m"
#define _blue_		"\033[34m"
#define _magenta_	"\033[35m"
#define _cyan_		"\033[36m"
#define _white_		"\033[37m"

#define _bold_		"\033[1m"
#define _italic_ 	"\033[3m"

#define _end_ 		"\033[0m"
