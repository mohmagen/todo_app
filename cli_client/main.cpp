#include "controller.hpp"
#include "model.hpp"
#include <arpa/inet.h>
#include <atomic>
#include <iostream>
#include <memory>
#include <netinet/in.h>
#include <ostream>
#include <socket/i_socket.hpp>
#include <socket/tcp_socket.hpp>
#include <socket/msg_socket.hpp>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <view.hpp>

int main() {	
	auto controller = ctrl::controller::new_atomic_controller();

	view::console(controller).start();
	model::model(controller).start();

	return 0;
}
