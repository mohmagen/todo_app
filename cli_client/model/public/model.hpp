#pragma once

#include "controller.hpp"
#include <memory>
#include <atomic>

namespace model {

	class model {
		public:
			model(ctrl::controller_ptr controller) : controller(controller) {}

			void start();
		private:
			ctrl::controller_ptr controller;
	};	
}
