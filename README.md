# Jabby - todo client server cli tool.


#### Available commands:
- __todo__ - commadns with _todo list_.
    - __todo-list__ - print _todo list_.
    - __todo-add__ (name) - add task to _todo list_.
    - __todo-start__ (ind) - start doing a task (make task in process state).
    - __todo-complete__ (ind) - complete task.
    - __todo-remove__ (ind) - remove task from todo list.
    - __todo-open__ (ind) - open task in editor (to view and change _description_).
    - __todo-description__ (ind) - print _description_ of task.

- __help__ - print help for _command_
- __exit__ - exit from todo app

- __server__ - commands to interact with _server_.
    - __server-connect__ (in_address, port) - connect to server.
    - __server-pull__ - get data from server. (automaticly run after connect to server)
    - __server-push__ - synchronise data with server. (automaticly run when exeit todo app)
    
