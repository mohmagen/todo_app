#include <iostream>
#include <server.hpp>

int main() {

	net::server server(net::server_config {8000, "127.0.0.1", 10});
	server.start();
	return 0;
}
