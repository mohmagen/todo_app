#pragma once 

#include <socket/complex_tcp_socket.hpp>
#include <iostream>
#include <unordered_map>
#include <string>
#include <memory>
#include <vector>
#include <thread>
#include <socket/i_socket.hpp>


namespace net {	
	struct server_config;

	class server {
			std::shared_ptr<soc::f_complex_tcp_server_socket_owner> server_socket;

		public:
			server(const server_config &config) noexcept;

			void start();
		private:	
			void recieve();
			void process();
			void send();
	};

	struct server_config {
		size_t port;
		const std::string &in_address;

		size_t max_users;
	};
}


#define _server_log_ "\033[32m[ server ]\033[0m"
#define _loop_ printf(_server_log_ " start loop.\n"); for (;;)
