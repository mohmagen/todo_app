
#include "socket/msg_socket.hpp"
#include <exception>
#include <ostream>
#include <socket/complex_tcp_socket.hpp>
#include <memory>
#include <server.hpp>
#include <socket/tcp_socket.hpp>
#include <socket/i_socket.hpp>
#include <string>
#include <stdio.h>

namespace net {
	server::server(const server_config &config) noexcept 
		: server_socket(new soc::f_complex_tcp_server_socket_owner(config.in_address, config.port, config.max_users)) {}
		
	void server::start() {
		printf(_server_log_ " init socet.\n");
		this->server_socket->init_soc();

		printf(_server_log_ " listen socet.\n");
		this->server_socket->listen_soc();

		auto conn = this->server_socket->create_connected(soc::e_connected_type::MSG);
		_loop_ {
			try {				
				const auto &result = dynamic_cast<const soc::msg_data&> (conn->read_soc());
				std::cout << _server_log_ 
					<< "name " << result.name 
					<< ", message \"" << result.message << "\"." 
					<< std::endl;
				
			} catch (std::exception *e) {
				std::cerr << _server_log_ << "error \"" << e->what() << "\"." << std::endl;
				break;
			}	
		}
	}

	void server::recieve() {
	}

	void server::process() {

	}

	void server::send() {

	}
}

