cmake_minimum_required(VERSION 3.27)

set(SERVER ${NAME}_server)
project(${SERVER})





# server folder
set(SERVER_LIB_NAME server_lib)
set(SERVER_LIB_DIR server)
set(SERVER_LIB_PUBLIC_DIR ${SERVER_LIB_DIR}/public)
set(SERVER_LIB_PRIVATE_DIR ${SERVER_LIB_DIR}/private)
add_library(${SERVER_LIB_NAME}
	${SERVER_LIB_PRIVATE_DIR}/lib.cpp
	${SERVER_LIB_PRIVATE_DIR}/server.cpp
)
target_include_directories(${SERVER_LIB_NAME}
	PUBLIC
		${SERVER_LIB_PUBLIC_DIR}
	PRIVATE
		${SERVER_LIB_PRIVATE_DIR}
)
target_link_libraries(${SERVER_LIB_NAME} socket)



# executable
add_executable(${SERVER} main.cpp)
target_link_libraries(${SERVER} server_lib)
set_target_properties(${SERVER}
	PROPERTIES
	ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/target/server/lib"
	LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/target/server/lib"
	RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/target/server/bin"
)
